# [HOWTO] Downgrade/update Lenovo Z40-75 Z50-75 notebook UEFI BIOS from DOS

## Discalmer
It might be risky to upgrade the firmware in any device. You can easily end up with a unusable device(aka brick).
*I personally not recommend to do it, if a system is stable you wouldn't win much with a newer FW.*
I told you, it's your responsibility if something goes wrong during the firmware update!
But life is not so easy can have pretty much the same experience with a [Windows update](https://www.forbes.com/sites/gordonkelly/2020/02/17/warnings-issued-for-millions-of-microsoft-windows-10-users/).

### Start the exe
You can download the update package from [manufacturer's site](https://pcsupport.lenovo.com/products/laptops-and-netbooks/lenovo-z-series-laptops/lenovo-z50-75/downloads/driver-list/).
*upgrade the firmware from Windows is generally a not-100%-safe move*
Click trought the well known next-next-finish combination. But, wait...

### I have a Linux distribution
on the main hard drive. I don't want to mess with Bill's creature.
But you might have an old and small USB flash drive with a bootable MS-DOS or [FreeDOS](https://www.freedos.org/).
I prepared all the binaries from Lenovo's package.

### Steps
* Fully charge the notebook, plug in both battery and the adapter,
* enter the bios and load the default settings (recommended),
* enable the BIOS Back Flash on the Configuration tab, if you want to downgrade (optional).
* Extract the archive to the root of the bootable DOS device,
* check the file integrity after the copy (strongly recommended)
* boot the DOS up,
* call Dosflash /help on the parent directory and
* take your time and read the options.
* I executed dosflash /bak oldbios.bin (to dump and update(!) the current UEFI and [EC](https://en.wikipedia.org/wiki/Embedded_controller) FW).
* the machine will reboot soon and start the upgrade procedure.
* After another reboot you should be able to enter the bios and check the version.

### Changelog [A4CN43WW](https://download.lenovo.com/consumer/mobiles/aclu7212.txt) (2018-01-18)
* Fine tune battery charge/discharge remaining time issue.
#### Download
[https://mega.nz/#!2B8W2AIA!iIqOkblQtfW0vOQfQ2UfPr8Vb5SnHKySLLvzPlRQ4ew](https://mega.nz/#!2B8W2AIA!iIqOkblQtfW0vOQfQ2UfPr8Vb5SnHKySLLvzPlRQ4ew)

### Changelog [A4CN42WW](https://download.lenovo.com/consumer/mobiles/aclu7211.txt) (2016-07-27)
* Fix UEFI Mantis issue No.1622 that DHCP boundary check issue.
* Fix UEFI Mantis issue No.1610 that Hypervisor security bypass issue.
* Fix UEFI Mantis issue No.1657 that SMM callout vulnerability issue.
* Fix unable to flash BIOS by Winflash after set HDD password.
#### Download
[TDB](about:blank)

### Changelog [A4CN40WW](https://download.lenovo.com/consumer/mobiles/a4cn40ww.txt) (2015-09-01)
* Exchange Lenovo new Logo.
* String changed “Win8 64bit/Other OS” to “Enabled/Disabled” at BIOS setting menu.
#### Download
[https://mega.nz/#!bNtA0QYK!CAr7oI8mcw3PPGsfHteyGDp5G50k7tZ11P5el5KZV3Q](https://mega.nz/#!bNtA0QYK!CAr7oI8mcw3PPGsfHteyGDp5G50k7tZ11P5el5KZV3Q)

### Changelog [A4CN33WW](https://download.lenovo.com/consumer/mobiles/a4cn33ww.txt) (2014-10-21)
* Add for UEFI security vulnerability issue1: Unauthorized Modification of UEFI Variables in UEFI Systems Enable secure BIOS.
* Add patch for UEFI security vulnerability issue2: UEFI EDK2 Capsule Update vulnerabilities.
#### Download
[https://mega.nz/#!fRUinYjQ!GIBWPwr8OOwhhaNrcPQ5to_4nl4OM9woEz7ybCTUiGg](https://mega.nz/#!fRUinYjQ!GIBWPwr8OOwhhaNrcPQ5to_4nl4OM9woEz7ybCTUiGg)

### Watch the full story here
[![IMAGE ALT TEXT](https://img.youtube.com/vi/G8LHTl9oV3s/maxresdefault.jpg)](https://www.youtube.com/watch?v=G8LHTl9oV3s "[HOWTO] Downgrade/Update Lenovo Z50-75 notebook UEFI BIOS from DOS")
