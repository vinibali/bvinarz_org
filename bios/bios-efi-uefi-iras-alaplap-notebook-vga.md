## BIOS, EFI, UEFI írás - alaplap || notebook || VGA
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/bios/src/bios.jpg)
### Vállalom BIOS chipek írását, felületszerelt vagy kivehető memória esetén.
#### Segítek:
* ha az íróprogram hibát jelzett
* ha elment az áram frissítés alatt
* ha lefagyott a gép írás közben
* ha rossz tartalom lett felírva
* ha megszakadt a frissítés
* vagy ha nem indul el a gép, a frissítés után
#### Rengeteg fajta tokozás esetén is szakszerű eljárás, akár forrasztás nélkül!
#### Hívjon vagy írjon: [emait](mailto:vinibali1@gmail.com).
#### Személyesen Budapesten, a XVIII. kerületben vagy postázással.
