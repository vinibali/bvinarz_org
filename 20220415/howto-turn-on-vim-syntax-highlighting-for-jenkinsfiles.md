# [HOWTO] Turn on VIM syntax highlighting for Jenkinsfiles
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220415/src/mypipeline.png)
Back in this week I was spending some time to write some pipeline definitions for Jenkins.
I'm using Vim all the time, so I had to take care of this missing function.

So you just need to add the following line to you ~/.vimrc:
```
autocmd BufNewFile,BufRead *[jJ]enkinsfile set syntax=groovy
```
and the text editor will automatically load the Groovy syntax highlighting for all the files called like:
* mypipeline.jenkinsfile
* mypipeline.Jenkinsfile

However you still have the chance to set this option for individual files with adding the [modeline magic](https://vimhelp.org/options.txt.html#modeline):

```
// vi:syntax=groovy
```

This is considered to be a security breach and being a disabled Vim function at many distributions, but it's definately working fine with Arch Linux :)
You can enable this with adding the following line to your ~/.vimrc:
```
set modeline
```
