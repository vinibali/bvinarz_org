# [HOWTO] Update to Gosund SP1 to OpenBeken (Tasmota alternative)
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20250105/src/1.jpg)

## CUCO Z0 R V1.2
There possibly a W701 chip under the cover, which is a RTL8720CF originally.

## Grab the binary
From [OpenBK7231T_App](https://github.com/openshwprojects/OpenBK7231T_App/releases) page.
You should take the one with RTL87X0C UART Flash this time.

## Pick up your soldering iron
*DO NOT POWER UP YOUR DEVICE FROM THE AC WHILE FLASHING*
You will need the following pads from the IOT module:
* I00 - connect to 3.3V VCC
* TXLOG - connect to your 3.3V USB-TTL adapter's RX
* RXLOG - connect to your 3.3V USB-TTL adapter's TX
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20250105/src/2.jpg)
You will need the following pads from the PCB:
* AMS1117's left leg (pin 1) - connect to GND
* AMS1117's middle leg (pin 2) - connect to 3.3V VCC
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20250105/src/3.png)

## Start the flashing using ltchiptool
[ltchiptool](https://github.com/libretiny-eu/ltchiptool)
```
$ ltchiptool flash write /media/ramdisk/OpenRTL87X0C_1.18.2.bin
I: Available COM ports:
I: |-- ttyUSB0 - FT232R USB UART - FT232R USB UART - FTDI (0403/6001)
I: |   |-- Selecting this port. To override, use -d/--device
I: |-- ttyS0 - ttyS0
I: Detected file type: Realtek AmebaZ2 Flash Image
I: Connecting to 'Realtek AmebaZ2' on /dev/ttyUSB0 @ 115200
I: Connect UART2 of the Realtek chip to the USB-TTL adapter:
I:
I:     --------+        +---------------------
I:          PC |        | RTL8720C
I:     --------+        +---------------------
I:          RX | ------ | TX2 (Log_TX / PA16)
I:          TX | ------ | RX2 (Log_RX / PA15)
I:             |        |
I:         GND | ------ | GND
I:     --------+        +---------------------
I:
I: Using a good, stable 3.3V power supply is crucial. Most flashing issues
I: are caused by either voltage drops during intensive flash operations,
I: or bad/loose wires.
I:
I: The UART adapter's 3.3V power regulator is usually not enough. Instead,
I: a regulated bench power supply, or a linear 1117-type regulator is recommended.
I:
I: In order to flash the chip, you need to enable download mode.
I: This is similar to ESP8266/ESP32, but the strapping pin (GPIO 0 / PA00)
I: has to be pulled *to 3.3V*, not GND.
I:
I: Additionally, make sure that pin PA13 (RX0) is NOT pulled to GND.
I:
I: Do this, in order:
I:  - connect PA00 to 3.3V
I:  - apply power to the device OR shortly connect CEN to GND
I:  - start the flashing process
I: |-- Success! Chip info: RTL8720CF
I: Writing '/media/ramdisk/OpenRTL87X0C_1.18.2.bin'
I: |-- Start offset: 0x0 (auto-detected)
I: |-- Write length: 720.8 KiB (auto-detected)
I: |-- Skipped data: 0 B (auto-detected)
  [################################################################]  100%          I: Transmission successful (ACK received).
I: Transmission successful (ACK received).

I: |-- Finished in 94.556 s
```

# Apply the template
```
{
  "vendor": "Gosund",
  "bDetailed": "0",
  "name": "Gosund SP1",
  "model": "SP1",
  "chip": "RTL8720CF",
  "board": "SP1-D-V1.5",
  "flags": "1024",
  "keywords": [
    "CUCO Z0 R V1.2",
    "TODO",
    "TODO"
  ],
  "pins": {
    "2": "WifiLED;0",
    "3": "Rel_n;0",
    "4": "Btn;0",
    "11": "Rel;0"
  },
  "command": "",
  "image": "http://www.gosund.net/img/SP1-D.b37bfdf4.png",
  "wiki": "https://www.elektroda.com/rtvforum/topic_YOUR_TOPIC.html"
}
```

## Enjoy
```
== Rtl8710c IoT Platform ==
Chip VID: 5, Ver: 3
ROM Version: v3.0

== Boot Loader ==
Aug 30 2024:04:22:00

Boot Loader <==

== RAM Start ==
Build @ 23:17:21, Jan  4 2025
interface 0 is initialized
interface 1 is initialized

Initializing WIFI ...Entering initLog()...
```
