# [COMPARE] BOSCH BMP280 and DHT12 temperature sensor accuracy

Back in time I ordered some DHT12 i2c modules for my ESP32 to playing around. In the last month I just had some time to play with those and it turned out, that this module is a not so good choice when you are using Tasmota.
Long story short, one of the guys mentioned that the DHT12 is just a random number generator and he advised to use a better one like BME280.
In my case I did a wrong order and managed to get 10 BMP280 which doesn't have the humidity sensor.

## Capabilities
Sensor|BME280|BMP280|DHT12|
---|---|---|---
Barometer|X|X|X
Humidity|X|O|X
Temperature|X|X|X

## For getting the data
I used two ESP32 devices side by side within a distance of 20 centimetres.
```
while true ; do curl -s "http://tasmota1.local/cm?cmnd=STATUS+8" | jq '[.StatusSNS.Time,.StatusSNS.BMP280.Temperature] | @csv' | sed 's/"//g ; s/\\//g '| tee -a ~/bosch.log ; sleep 1800s ; done
while true ; do curl -s "http://tasmota2.local/cm?cmnd=STATUS+8" | jq '[.StatusSNS.Time,.StatusSNS.DHT12.Temperature] | @csv' | sed 's/"//g ; s/\\//g ; s/,/;/g ; s/\./,/g' | tee -a ~/dth.log ; sleep 1800s ; done
```

## So tell me the numbers!
I'm so suprised to see, that the difference is 1°C as a worse value between these two sensors.
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20230827/src/1.png)
In this case we are talking about a warmer indoor application as you can see.
