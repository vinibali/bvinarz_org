# [COMPARE] Parkside X20V Team battery chargers

These two chargers were shipped with two pack of PARKSIDE PABS 20-Li E6 cordless hand drills.
The one I brought a year later, had the balanced battery pack and the C1 charger, otherwise both look very similar.
## PLG20C1 on the left, PLC20A1 on the right

So here is a picture about the top side, nothing special just the facelifted C1 version on the left. The leds are behind some transparent plastic, IMHO that looks a little bit better.
![PLG20C1 on the left, PLC20A1 on the right](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_184656.jpg)

The next one is from the bottom, the guys at the factory gave us two holes, which you can use to hang on the charger. Such a great idea!
![PLG20C1 on the left, PLC20A1 on the right](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_184715.jpg)

You can see the circuit boards now, the C1 version might contain smallers parts. Some of the hole mounted capacitors have been changed to SMD ones. LGT695G LY758GX.
![PLG20C1 on the left, PLC20A1 on the right](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_185056.jpg)
![PLG20C1 on the left, PLC20A1 on the right](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_185232.jpg)

Both the chargers are using a LGT8P22A balancer/controller/protection(?) chip and a 4953 P-channel dual MOSFET. I haven't found any data about the IC, I you do so please leave a message.
![LGT8P22A](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_185421.jpg)

The power cables are the same Dinatong DNT-11, it's a pretty flexible and good quality one.
![Dinatong DNT-11](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_185617.jpg)

The weight of the empty plastic housing are pretty much the same, they are 153g for the PLG20A1 and 151g for the PLG20C1!
![Empty plastic housing PLG20A1 153g](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_190230.jpg)
![Empty plastic housing PLG20C1 151g](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_190245.jpg)

Almost the same difference can be seen with the complete chargers too, 309 gramms for the PLG20A1 and 306 gramms for the PLG20C1.
![Complete charger PLG20A1 309g](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_190419.jpg)
![Complete charger PLG20C1 306g](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220812/src/IMG_20220810_190642.jpg)

[LGT8P22A](https://prohardver.hu/tema/hobby_elektronika/hsz_76845-76845.html)
[LGT8P22A](https://www.elektroda.com/rtvforum/topic3691907.html)
