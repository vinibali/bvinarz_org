# [DIY] 3D printed Scythe Shuriken fan duct adapter for 12cm fan

## Intro
I used to use top-down CPU coolers over the time, to keep the VRM and memories cooler with a little bit of airflow.

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20210611/src/1.jpg)
[Image source: Optimized CPU Cooling with Top-Down Heatsinks](https://techplayboy.com/30111/optimized-cpu-cooling-performance-with-top-down-heatsinks/2/)

In the past on my AMD FMx platforms the cooler was a Cooler Master GeminII M4 along with a super silent Coolink Swif2 or Scythe S-FLEX over the years. BTW both the fans has a vey long lifetime, thanks to the hidro bearing.
Using the APU's integrated graphics with a very silent cooling solution is the way to go. You can eliminate all the heat with only one fan.
So back in 2019 I had a great change to upgrade my main system and make a move to the AM4 platform.
AMD has changed the socket and increased the number of pins of the processor. This time they had to change the dimensioning of the cooling solution's fastener backplate too. This move has made the coolers no longer usable, which uses custom backplates. The coolers, which use the factory fastening system can still be used. For example if I have an age old AM2 cooler, you can still use it with a shiny Ryzen platform, but that's not the case with the GeminII M4.

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20210611/src/2.jpg)
[Image source: Differences in the fastening system Socket AM4](http://xtreview.com/addcomment-id-46524-view-Differences-in-the-fastening-system-Socket-AM4.html)

## Solution
I needed a decent cooler, because the stock AMD Wraith Stealth cooler was more noisy and CPU run on higher temperature, than what I used to see on the system tray.
Some old 1st gen Scythe Shuriken was laying around, but it was using a 10cm sleeve bearing fan, which of course has that bearing whine. So I decided to design a CAD model which adapts to the 12 cm fan and the 10 cm heatsink and after that, just print it with my Tronxy P802.
You can see 4 notches and hump on the 2 parts, which makes it easier to align and glue them together. I've glued mine with a well know universal high strenght one and it's been together for 1,5 years. Don't be afraid of this method.

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20210611/src/3.jpg)

## Printing and set-up

Watch the full video here, click on the image:
[![IMAGE ALT TEXT](https://img.youtube.com/vi/kiaIC72dmG0/maxresdefault.jpg)](https://www.youtube.com/watch?v=kiaIC72dmG0 "[DIY] 3D printed Scythe Shuriken fan duct adapter for 12cm fan")

## Results

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20210611/src/4.png)
I measured the idle temperature, while I changed the fan. The overall temperature has dropped around 2 degrees Celsius under 5-6 minutes. I think some more improvement has been archived, the bad airflow has increased the area's overall temperature, which might needs more time to cool down.

## Grab the STL

[12cm cooling fan extender for Scythe Shuriken](https://www.thingiverse.com/thing:3972660)
