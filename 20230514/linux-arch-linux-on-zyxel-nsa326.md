# [LINUX] Arch Linux on Zyxel NAS326

## You will need the following tools to have Arch Linux installed on you NAS:
* 3,3V serial adater
* serial terminal
* some USB flashdrive
* device tree source: [armada-380-zyxel-nas326.dtb]((https://gitlab.com/vinibali/bvinarz_org/raw/master/20230514/src/armada-380-zyxel-nas326.dtb)
 

## Why isn't this devices supported by default?
Long story short: the ALARM community supports some devices, for which you can grab the device related install instructions and the rootfs including the boot files.
If you want to use something, which is not officially supported, you can download the general ARMv7 rootfs(called Multi-platform) and create your own installation.

## Prepare the OS
In order to archive this, you'll need to:
* download the rootfs archive: [http://os.archlinuxarm.org/os/ArchLinuxARM-armv7-latest.tar.gz](http://os.archlinuxarm.org/os/ArchLinuxARM-armv7-latest.tar.gz)
* prepare the flashdrive, open up the instructions for ClearFog: [Micro SD Card Creation](https://archlinuxarm.org/platforms/armv7/marvell/clearfog) _I hate to replicate something, what can be changed over time_
* follow the steps from 1->6 *STOP BEFORE STEP 7*
* create the so-called Marvell specific boot image
* this case we'll need a bundled kernel uImage which holds the dts at the end
```
$ cd /boot
# cp -a zImage zImage.fdt
# cat dts/armada-380-zyxel-nas326.dtb >> zImage.fdt
# mkimage -A arm -O linux -T kernel -C none -a 0x00008000 -e 0x00008000 -n ArchLinux -d zImage.fdt uImage
$ binwalk zImage zImage.fdt uImage
Scan Time:     2023-05-14 10:44:48
Target File:   /boot/zImage
MD5 Checksum:  61ec3def8031e8dfa4b531744c3e3891
Signatures:    411
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Microsoft executable, portable (PE)
54524         0xD4FC          xz compressed data
57926         0xE246          xz compressed data
Scan Time:     2023-05-14 10:45:01
Target File:   /boot/zImage.fdt
MD5 Checksum:  bbb61f9e819b890c9c5b4a8e8a8aae62
Signatures:    411
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Microsoft executable, portable (PE)
54524         0xD4FC          xz compressed data
57926         0xE246          xz compressed data
7864832       0x780200        Flattened device tree, size: 20586 bytes, version: 17
Scan Time:     2023-05-14 10:45:01
Target File:   /boot/uImage
MD5 Checksum:  a0cee20a911a221e5464650a2ad2b36e
Signatures:    411
DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             uImage header, header size: 64 bytes, header CRC: 0xD81ECCE9, created: 2023-04-11 11:13:28, image size: 7885418 bytes, Data Address: 0x8000, Entry Point: 0x8000, data CRC: 0xD2B98796, OS: Linux, CPU: ARM, image type: OS Kernel Image, compression type: none, image name: "ArchLinux"
64            0x40            Microsoft executable, portable (PE)
54588         0xD53C          xz compressed data
57990         0xE286          xz compressed data
7864896       0x780240        Flattened device tree, size: 20586 bytes, version: 17
```

## Open up the device and connect the serial port
You can use modernhacker's website for the reference pictures:
[Install Debian / Linux into Zyxel 326 NAS](http://modernhackers.com/install-debian-linux-into-zyxel-326-nas/)
```
Zyxel NAS326 Serial Pinouts (same as NSA325 and NSA310S/320S)

GND
RX
TX

          +----+----+
          |    |    |
+----+----+----+----+----+
|3.3V| TX | RX |    | GND|
+----+----+----+    +----+
```

## Backup the U-Boot environment variables with printenv:
```
BootROM: Image checksum verification PASSED

 __   __                      _ _
|  \/  | __ _ _ ____   _____| | |
| |\/| |/ _` | '__\ \ / / _ \ | |
| |  | | (_| | |   \ V /  __/ | |
|_|  |_|\__,_|_|    \_/ \___|_|_|
         _   _     ____              _
        | | | |   | __ )  ___   ___ | |
        | | | |___|  _ \ / _ \ / _ \| __|
        | |_| |___| |_) | (_) | (_) | |_
         \___/    |____/ \___/ \___/ \__|
 ** LOADER **


U-Boot 2013.01-svn48206 (Dec 03 2015 - 02:50:37) Marvell version: 2014_T3.0p6

Board: RD-NAS-88F6820-DDR3
SoC:   MV88F6810 Rev A0
       running 1 CPUs
CPU:   ARM Cortex A9 MPCore (Rev 1) LE
       CPU 0
       CPU    @ 1332 [MHz]
       L2     @ 666 [MHz]
       TClock @ 200 [MHz]
       DDR    @ 666 [MHz]
       DDR 32 Bit Width, FastPath Memory Access, DLB Enabled, ECC Disabled
DRAM:  512 MiB

Map:   Code:                    0x1fed0000:0x1ff95e28
       BSS:                     0x1ffef2b4
       Stack:                   0x1f9cff20
       Heap:                    0x1f9d0000:0x1fed0000
       U-Boot Environment:      0x00200000:0x00280000 (NAND)

NAND:  256 MiB
MMC:   mv_sdh: 0
*** Booting kernel from kernel 1 @0x00000000; run bootcmd_custom;  ***
USB2.0 0: Host Mode
USB3.0 0: Host Mode
USB3.0 1: Host Mode
Board configuration detected:
Net:
|  port  | Interface | PHY address  |
|--------|-----------|--------------|
| egiga0 |   RGMII   |     0x01     |
egiga0 [PRIME]
Hit any key to stop autoboot:  0
Marvell>> printenv
```

## Modify the following U-Boot environment variables with setenv and saveenv:
```
setenv curr_bootfrom 1
setenv next_bootfrom 1
setenv bootargs console=ttyS0,115200 root=/dev/sda1 rootfstype=ext4 rootdelay=1 earlyprintk=serial
setenv bootcmd nand read 0x2000000 0x00000000; run bootcmd_custom;  0xF00000 && bootz 0x2000000
setenv bootcmd_auto stage_boot $boot_order
setenv bootcmd_custom if run usb_bootcmd; then; else if run bootcmd_stock_1; then; else run bootcmd_stock_2; reset; fi; fi
setenv usb_bootcmd echo Booting from USB ...; setenv fdt_skip_update yes; run usb_init; ext4load usb 0:1 $load_image_addr uImage; bootm $load_image_addr
setenv usb_init mw.l f1018100 20420000; mw.l f1018140 003E8800; sleep 3; usb start
Marvell>> saveenv
Saving Environment to NAND...
Erasing Nand...
Writing to Nand... done
Marvell>> reset
resetting ...
```
*NOTE: in this case we are not using the initrd, just the uImage!*
```
Booting from USB ...
(Re)start USB...
USB0:   Port (usbActive) : 0    Interface (usbType = 2) : USB EHCI 1.00
scanning bus 0 for devices... 2 USB Device(s) found
       scanning usb for storage devices... 1 Storage Device(s) found
       scanning usb for ethernet devices... 0 Ethernet Device(s) found
7815850 bytes read in 366 ms (20.4 MiB/s)
## Booting kernel from Legacy Image at 02000000 ...
   Image Name:   archlinuxkernel
   Created:      2023-02-14  13:56:41 UTC
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    7815786 Bytes = 7.5 MiB
   Load Address: 00008000
   Entry Point:  00008000
   Verifying Checksum ... OK
   Loading Kernel Image ... OK
OK

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 5.17.1-1-ARCH (builduser@leming) (armv7l-unknown-linux-gnueabihf-gcc (GCC) 11.2.0, GNU ld (GNU Binutils) 2.38) #1 SMP PREEMPT Mon Mar 28 23:55:2
```

### Please make your feedback
I wrote this page mostly from the saved serial logs and remembering the day, when I did this.
Some parts might be inaccurate, please do your contribution and let me know if you find anything!

### Special thanks to
This tutorial is highly based on the:
Doozan tutorial: [Zyxel NAS326 Installation Instruction](https://forum.doozan.com/read.php?2,88619,page=1)
Modernhackers tutorial: [Install Debian / Linux into Zyxel 326 NAS](http://modernhackers.com/install-debian-linux-into-zyxel-326-nas/)
