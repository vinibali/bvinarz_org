# How much time IONOS needs to terminate your subscription?
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20221202/src/1.png)

## Cheap hosting with SSH
Back in last year, I was looking for to move my domain to another hosting company.
I just realized that, both German and US IONOS provides hosting service for 1€/mon which is quiet good for the first year.
Later on it turned out, those locations are far away from what I'm doing and after a year, I decided to cancel the subscription.

### Debian 10 containers on the go
Great service, with a somewhat better configured "VPS" I've seen so far.
I'm pretty sure most of the jobs are very well automated, however looking at the new contacts are definately something what they are doing manually. I remember, when I brought the service on both companies and some hours later they terminated both of them because of some typo in my address.

## But really how much time it took after they removed my access?
You need to confirm your will to terminate the subscription, in a phone call. I remember the colleague said they will remove everything right away and I'll have no longer access to the VPS.

## Let the counter start
```
OPI $ tmux
OPI $ ssh ionos-us
ION $ while true ; do date && sleep 60 ; done
Fri Dec  2 08:33:04 EST 2022
.
.
.
Fri Dec  2 12:07:04 EST 2022
Connection to access.webspace-data.io closed by remote host.
Connection to access.webspace-data.io closed.
ION $
```
I recived the confirmation e-mail at 8:33 EST, so it took around _3,5 hours_ to remove my access from the hosting machines.
Pretty impressive, if that's really automated.
