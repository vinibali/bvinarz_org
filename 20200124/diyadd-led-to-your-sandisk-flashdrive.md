#[DIY]Add LED to your SanDisk flashdrive

Manufacturers always tries to ~~maximize the profit~~ reduce the overall cost of their products.
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20200124/src/1.jpg)

### Industry practices
Back in 2014 Kingston decided to switch away from the blazing fast 19nm Toshiba MLC NAND in the V300 Series SSD.
Nearly one year after they introduced this - consumer/value level - SSDNow Sandforce series, they just bumped the firmware version and exchanged the flash chips under the hood.

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20200124/src/2.jpg)
Most of the time the SF2281 contoller is able to compress - with Gzip - the data on the fly and writes less into the flash chips. This trick could hide the speed difference between the async and sync memory chips, if you are saving compressable files to the partition, like plain text files, documents, [ELFs](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format) and so on.
If you want to know more about Kingston's move, please visit Anandtech's analysis: [An Update to Kingston SSDNow V300: A Switch to Slower Micron NAND](https://www.anandtech.com/show/7763/an-update-to-kingston-ssdnow-v300-a-switch-to-slower-micron-nand).

### The flash drive situation
There are other different ways to optimize the cost of manufacturing.
You can:
* outsource the manufacturing
* rebrand some OEM/ODM product
* use cheaper NAND(again)
* rebrand an old, already designed product
* simplify the PCB
* simplify the housing
* simplify the packaging
* change the internal part(s) to a monolithic flash device
* miss out the only LED diode and it's ballast resistior which costs 0.001 $

### Why is the LED needed
You might have a last chance live system on your pocket, but the host machine is really slow and dying.
If you have an LED on the flash drive you'll know if at least some I/O operation is going on.
There are different approches in each manufacturer, let me share three examples with you.
The following two options are not very useful unless your want to test the voltage on the USB plug itself.
* brand A loves to just blink the LED which shows, that your drive is powered
* brand B loves to just light the LED which shows, that your drive is powered
If you really want to know the activity on your external drive, you should buy from:
* brand C, who loves if the LED blinks according the data transfer between the host, controller and flash chip.

#### Sandisk Cruzer Blade 16GB
is a value-oriented USB2.0 drive. It's available on the market for 10 years, I found a couple of reviews from 2010.
I brought my drive back in 2015 and it's PCB revision seems to be from the year 2012.
If your drive don't has an LED, mostly this part is missing from the biggest SMD mount pad.

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20200124/src/3.jpg)
[Older drive with the LED with PCB version 2010](https://goughlui.com/2015/07/31/project-physically-damaged-usb-drive-repair-data-recovery/).

#### I brought my drive back in 2015
It spent the last three years in a USB port on some OpenWRT router.
But now I needed a relatively small drive to install a pretty dumb and minimal, CLI only Arch Linux installation. So I upgraded the flash drive on the TP-Link gateway to give this a go. The installation was pretty slow, but I'm not planning to upgrade it every single day, open the LibreOffice or use some fancy KDE desktop environment on it.
The system I used to use it is a headless AM2 Asus motherboard and I really wanted to know weather it boots or not. I didn't want to search for an external VGA, keyboard and display to have a look whats going on.
If this flash drive could have an LED on it, I can easily check weather it's booting or not.

#### Open up the housing
was so easy. You just have to squeeze a bit in the sides and put you nail into the gap, where the red and black plastic pieces are connected. Be gentle during the disassembly, there are no screws or glue between the parts.
You can see the PCB below with SanDisk's own controller, a couple of capacitors, resistors and maybe a clock generator crystal.
![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20200124/src/4.jpg)

#### Just grab a multimeter and soldering iron
and you are good to go. I used the image from Gough's Tech Zone to figure out where should I place the LED. As I mentioned, if it's missing, than you'll need to place it into the biggest SMD mount point on the PCB. This time the LED is connected to 3,5 Volts with a resistor and a GPIO leg is controlls the way of working with grounding this leg.
It's not my life's best soldering I know. But belive me, the components are damn small.

![img](https://gitlab.com/vinibali/bvinarz_org/raw/master/20200124/src/5.jpg)
Equipped with 0,0089$ orange LED
