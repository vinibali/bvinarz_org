# ASRock A75-Pro4-M beta BIOS

![A75 Pro4-M](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220212/src/13-157-259-04.jpg)
[image source](https://c1.neweggimages.com/ProductImageCompressAll1280/13-157-259-04.jpg)

Back in the [FM1](https://en.wikipedia.org/wiki/Socket_FM1) times, I was so lucky to own this great motherboard.
This was one of the very few boards, which was able to use two digital ouputs at the same time.

I'd like to share with you the latest, publicly never available beta bios, which I've got from the ASRock support back in time.
```
Update: Add eip84455 to improve KB/MS compatibility.
```

I don't really understand the technical details behind the improvement, but this might be another check if the connected device is a:
* mouse
* keyboard
* hub
* storage
* point
* ccid



[A75P4M191.bin](https://gitlab.com/vinibali/bvinarz_org/raw/master/20220212/src/A75P4M191.bin)

sha256sum: 8d7583f3ced4aca041cb45a76df5902d64e91a7fc517b0e76f5824440460aca8

The only found code snippet for this keyword(EIP84455)

```c
VOID
ReportDevices(
    IN OUT CONNECTED_USB_DEVICES_NUM    *Devs
)
{
											//(EIP38434)>
	UINT8	i,Kbd = 0, Hub = 0, Mouse = 0, Mass = 0, Point = 0, Ccid = 0;

	for (i = 1; i<MAX_DEVICES; i++) {

		if ((gUsbData->aDevInfoTable[i].bFlag & DEV_INFO_VALIDPRESENT)
			!= DEV_INFO_VALIDPRESENT) {
			continue;
		}
		switch(gUsbData->aDevInfoTable[i].bDeviceType)
		{
			case BIOS_DEV_TYPE_HID:
                                        //(EIP84455+)>
                    if(gUsbData->aDevInfoTable[i].bSubDeviceType& SUB_DEV_TYPE_KEYBOARD)
                        Kbd++;
                    if(gUsbData->aDevInfoTable[i].bSubDeviceType & SUB_DEV_TYPE_MOUSE)
                        Mouse++;
                    if(gUsbData->aDevInfoTable[i].bSubDeviceType & SUB_DEV_TYPE_POINT)
                        Point++;
                                        //<(EIP84455+)
				break;

			case BIOS_DEV_TYPE_HUB:
				Hub++;
				break;

			case BIOS_DEV_TYPE_STORAGE:
				Mass++;
				break;

			case BIOS_DEV_TYPE_CCID:
				Ccid++;
				break;
		}
	}
```

source: [Programmer ITS203_uefi library function](https://its203.com/article/robinsongsog/42396837)
