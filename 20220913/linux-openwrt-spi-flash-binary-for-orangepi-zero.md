# [LINUX] OpenWRT SPI flash binary for OrangePI Zero
![img](https://gitlab.com/vinibali/bvinarz_org/-/raw/master/20220913/src/luci.png)
I've been thinking about to use a very minimal, XOR flash only Linux OS for my OrangePIs.
Building up a system on MTD like devices are somewhat similiar to the way how we are using partitions on MMCs, SSDs, HDDs and so on. Most of the time, the Memory Technology Device subsystem is used, when you are working with raw flash devices, like NAND, NOR, XOR, etc...

## /dev/{mtd,nvme,sda,hda}
You can read more the differences between a flash and block devices at Infradead's webpage:
[What are the differences between flash devices and block drives?](http://www.linux-mtd.infradead.org/faq/general.html#L_mtd_vs_hdd)
This is how the MTD partitions look like:
```
root@openwrt-orangepi-zero:~# cat /proc/mtd
dev:    size   erasesize  name
mtd0: 00080000 00010000 "uboot"
mtd1: 00010000 00010000 "dtb"
mtd2: 00f70000 00010000 "firmware"
mtd3: 002a6910 00010000 "kernel"
mtd4: 00cc96f0 00010000 "rootfs"
mtd5: 000d0000 00010000 "rootfs_data"
```

![img](https://gitlab.com/vinibali/bvinarz_org/-/raw/master/20220913/src/boot-config.png)
So luckily there is a guy in Russia called [tr4bl3](http://4pda.ru/forum/index.php?showuser=1177645) and he is the only who built up a very featureful OpenWRT image for the OrangePI Zero devices. This build has tons of features, like SPDIF, I2C, webradio, GPIO and even a somewhat working WiFi.

### _Hint_:
There are many forks for the XR819 wireless around there, maybe the most used are [https://github.com/fifteenhex/xradio](https://github.com/fifteenhex/xradio). AFAIR Armbian is pretty up to date with the Xradio driver and firmware.

## Full image dump
This time, I would like to share the 128Mbit raw dump with you from the SPI flash. In order to flash this image, you need to get access to the SPI flash device at the device tree source:
[How to enable SPI on Orange PI PC+ in Armbian?](https://unix.stackexchange.com/questions/496070/how-to-enable-spi-on-orange-pi-pc-in-armbian)
*Grab your image here:*
[openwrt_orangepizero_w25q128.bin](https://gitlab.com/vinibali/bvinarz_org/-/raw/master/20220913/src/openwrt_orangepizero_w25q128.bin)
sha256sum 78a5a0b0daa15f17f16af8e2227f58b2c60b161db8b147d2b281a86b2f6c3b71

### You can find some more story about the device tree in my other post too:
[DIY OrangePI Zero SPDIF](https://www.bvinarz.org/20210523/diy-orangepi-zero-spdif/)

## Write the image with flashrom,
will take some time, the speed of the SPI interface is very limited here. I don't really tested what could be the highest speed. Please note, there are two SPI devices at this platform, one is the soldered one and the other is at the jumpers!
```
time sudo flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=10000 -w openwrt.bin
flashrom v1.2 on Linux 5.11.2-1-ARCH (armv7l)
flashrom is free software, get the source code at https://flashrom.org

Using clock_gettime for delay loops (clk_id: 1, resolution: 1ns).
Found Winbond flash chip "W25Q128.V" (16384 kB, SPI) on linux_spi.
Reading old flash chip contents... done.
Erasing and writing flash chip... Erase/write done.
Verifying flash... VERIFIED.

real	9m50.602s
user	6m21.807s
sys	1m18.650s
```

## The boot order in the Allwinner SoCs are always the eMMC->SPI, so you'll need to remove the sdcard before booting up the SPI flash chip.

```
U-Boot SPL 2020.04 (Feb 15 2021 - 15:22:37 +0000)
DRAM: 512 MiB
Trying to boot from sunxi SPI


U-Boot 2020.04 (Feb 15 2021 - 15:22:37 +0000) Allwinner Technology

CPU:   Allwinner H3 (SUN8I 1680)
Model: Xunlong Orange Pi Zero
DRAM:  512 MiB
MMC:   mmc@1c0f000: 0, mmc@1c10000: 1
Loading Environment from FAT... MMC: no card present
In:    serial
Out:   serial
Err:   serial
Net:   phy interface0
eth0: ethernet@1c30000
starting USB...
Bus usb@1c1a000: USB EHCI 1.00
Bus usb@1c1a400: USB OHCI 1.0
Bus usb@1c1b000: USB EHCI 1.00
Bus usb@1c1b400: USB OHCI 1.0
scanning bus usb@1c1a000 for devices... 1 USB Device(s) found
scanning bus usb@1c1a400 for devices... 1 USB Device(s) found
scanning bus usb@1c1b000 for devices... 1 USB Device(s) found
scanning bus usb@1c1b400 for devices... 1 USB Device(s) found
       scanning usb for storage devices... 0 Storage Device(s) found
Hit any key to stop autoboot:  0 
MMC: no card present
SF: Detected w25q128 with page size 256 Bytes, erase size 4 KiB, total 16 MiB
--- SPI OK ---
device 0 offset 0x80000, size 0x10000
SF: 65536 bytes @ 0x80000 Read: OK
device 0 offset 0x90000, size 0x400000
SF: 4194304 bytes @ 0x90000 Read: OK
## Booting kernel from Legacy Image at 42000000 ...
   Image Name:   ARM OpenWrt Linux-4.14.221
   Image Type:   ARM Linux Kernel Image (uncompressed)
   Data Size:    2779344 Bytes = 2.7 MiB
   Load Address: 40008000
   Entry Point:  40008000
   Verifying Checksum ... OK
## Flattened Device Tree blob at 43000000
   Booting using the fdt blob at 0x43000000
EHCI failed to shut down host controller.
   Loading Kernel Image
   Using Device Tree in place at 43000000, end 43008ff0

Starting kernel ...

[    0.000000] Booting Linux on physical CPU 0x0
[    0.000000] Linux version 4.14.221 (sem@RV508) (gcc version 7.5.0 (OpenWrt GCC 7.5.0 r11306-c4a6851c72)) #0 SMP PREEMPT Mon Feb 15 15:22:37 2021
[    0.000000] CPU: ARMv7 Processor [410fc075] revision 5 (ARMv7), cr=30c5387d
[    0.000000] CPU: div instructions available: patching division code
[    0.000000] CPU: PIPT / VIPT nonaliasing data cache, VIPT aliasing instruction cache
[    0.000000] OF: fdt: Machine model: Xunlong Orange Pi Zero
[    0.000000] Memory policy: Data cache writealloc
[    0.000000] psci: probing for conduit method from DT.
[    0.000000] psci: Using PSCI v0.1 Function IDs from DT
[    0.000000] random: get_random_bytes called from start_kernel+0x88/0x3c0 with crng_init=0
[    0.000000] percpu: Embedded 15 pages/cpu s30732 r8192 d22516 u61440
[    0.000000] Built 1 zonelists, mobility grouping on.  Total pages: 130048
[    0.000000] Kernel command line: console=ttyS0,115200 earlyprintk rootfstype=squashfs mtdparts=spi0.0:512k(uboot),64k(dtb),-(firmware)
[    0.000000] PID hash table entries: 2048 (order: 1, 8192 bytes)
[    0.000000] Dentry cache hash table entries: 65536 (order: 6, 262144 bytes)
[    0.000000] Inode-cache hash table entries: 32768 (order: 5, 131072 bytes)
[    0.000000] Memory: 508488K/524288K available (5563K kernel code, 395K rwdata, 1624K rodata, 2048K init, 246K bss, 15800K reserved, 0K cma-reserved, 0K highmem)
[    0.000000] Virtual kernel memory layout:
[    0.000000]     vector  : 0xffff0000 - 0xffff1000   (   4 kB)
[    0.000000]     fixmap  : 0xffc00000 - 0xfff00000   (3072 kB)
[    0.000000]     vmalloc : 0xe0800000 - 0xff800000   ( 496 MB)
[    0.000000]     lowmem  : 0xc0000000 - 0xe0000000   ( 512 MB)
[    0.000000]     pkmap   : 0xbfe00000 - 0xc0000000   (   2 MB)
[    0.000000]     modules : 0xbf000000 - 0xbfe00000   (  14 MB)
[    0.000000]       .text : 0xc0008000 - 0xc076ef28   (7580 kB)
[    0.000000]       .init : 0xc0a00000 - 0xc0c00000   (2048 kB)
[    0.000000]       .data : 0xc0c00000 - 0xc0c62fc0   ( 396 kB)
[    0.000000]        .bss : 0xc0c69238 - 0xc0ca6b90   ( 247 kB)
[    0.000000] SLUB: HWalign=64, Order=0-3, MinObjects=0, CPUs=4, Nodes=1
[    0.000000] Preemptible hierarchical RCU implementation.
[    0.000000]  RCU restricting CPUs from NR_CPUS=8 to nr_cpu_ids=4.
[    0.000000]  Tasks RCU enabled.
[    0.000000] RCU: Adjusting geometry for rcu_fanout_leaf=16, nr_cpu_ids=4
[    0.000000] NR_IRQS: 16, nr_irqs: 16, preallocated irqs: 16
[    0.000000] GIC: Using split EOI/Deactivate mode
[    0.000000] clocksource: timer: mask: 0xffffffff max_cycles: 0xffffffff, max_idle_ns: 79635851949 ns
[    0.000000] arch_timer: cp15 timer(s) running at 24.00MHz (phys).
[    0.000000] clocksource: arch_sys_counter: mask: 0xffffffffffffff max_cycles: 0x588fe9dc0, max_idle_ns: 440795202592 ns
```

## You can build your image
and use an alternative way to write the MTD partitions one by one, if you go to Melsem's GitHub page:
[https://github.com/melsem/openwrt-19.07.7-spi](https://github.com/melsem/openwrt-19.07.7-spi)
