# [HOWTO] Unbrick your ESP32 after an interrupted update with Tuya-convert

## Tuya-convert
is an open source project baked by VTRUST GmbH, which allows you to turn you Tuya devices to something more secure without opening up the device.

## First, I gave it a try with my Armbian on the OrangePI PC I have.
Unfortunately this was no the best idea, I *recommend you to use some older Ubuntu releases, like 18.04*!

## SmartConfig was running many times,
without any success and I decided to unplug the device. This was a mistake for sure. After that:
* the led flashed once when I plugged in the device but,
* the buttos was not working, because
* the transition firmware was unconfigured
Do the button was no working, I could not reset the device into pairing mode again.
```
Put device in EZ config mode (blinking fast)
Sending SSID                  vtrust-flash
Sending wifiPassword
Sending token                 00000000
Sending secret                0101
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
..........
SmartConfig complete.
Resending SmartConfig Packets
```
## Follow the smarthack-wifi.log
After I plugged in and off the device couple of times, I realized that the button and the LED are not working as supposed, however the plug connects to the Tuya-conver Access Point.
```
Attempting to stop wpa_supplicant
Stopping NetworkManager...
Configuring AP interface...
RTNETLINK answers: File exists
Starting DNSMASQ server...
Starting AP on wlan0...
wlxc46e1f23a9a0: interface state UNINITIALIZED->ENABLED
wlxc46e1f23a9a0: AP-ENABLED
wlxc46e1f23a9a0: AP-STA-CONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-DISCONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-CONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-DISCONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-CONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-DISCONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-CONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-DISCONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-CONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-DISCONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-CONNECTED 8C:CE:4E:XX:XX:XX
wlxc46e1f23a9a0: AP-STA-DISCONNECTED 8C:CE:4E:XX:XX:XX
```

## The static configuration
was in place, the transition firmware was connecting to the WiFi AP every time.
_Note: I was scanning the AP's subnet in this case!_
```
$ nmap 10.42.42.0/24
Starting Nmap 7.80 ( https://nmap.org ) at 2022-12-27 13:31 CET
Nmap scan report for 10.42.42.1
Host is up (0.00045s latency).
Not shown: 996 closed ports
PORT    STATE SERVICE
22/tcp  open  ssh
53/tcp  open  domain
80/tcp  open  http
443/tcp open  https

Nmap scan report for 10.42.42.42
Host is up (0.0015s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap done: 256 IP addresses (2 hosts up) scanned in 10.55 seconds
```

## VTRUST-FLASH 1.5
Let's roll back to the previous firmware with simple curl commands.
```
$ curl 10.42.42.42
VTRUST-FLASH 1.5
(c) VTRUST GMBH https://www.vtrust.de/35c3/
READ FLASH: http://10.42.42.42/backup
ChipID: xxxxxxx
MAC: 8C:CE:4E:xx:xx:xx
BootVersion: 7
BootMode: normal
FlashMode: 1M DOUT @ 40MHz
FlashChipId: 144051
FlashChipRealSize: 1024K
Active Userspace: user1 0x01000
$ curl 10.42.42.42/backup
Warning: Binary output can mess up your terminal. Use "--output -" to tell 
Warning: curl to output it to your terminal anyway, or consider "--output 
Warning: <FILE>" to save to a file.
$ curl 10.42.42.42/backup --output 1.bin
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 1024k  100 1024k    0     0  72555      0  0:00:14  0:00:14 --:--:-- 32725
$ curl 10.42.42.42/undo

Rebooting into userspace 2
```

## The factory firmware was working again,
I could jump the device into pairing mode.
*This is a really lucky scenario in my case, yours might be different. Not all devices are saveable!*

## CAUTION:
you can easily brick your device. If you are unsure about what you are doing as some of the local guys from the community.

## Do you own research.
Use the Reddit, GitHub, Discord etc channels to get the information.
As I experience Tuya-convert's script is not so foolproof, do not try to use in on your Arch Linux ARM for sure :)
